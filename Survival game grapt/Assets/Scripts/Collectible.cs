﻿using UnityEngine;

public class Collectible : MonoBehaviour
{
    
    public string typeUpdate = "";
    public int value = 1;
    public Transform Player;

    void Start()
    {
    }

    private void OnTriggerEnter()
    {
        Debug.Log("collision");
        Player p = Player.GetComponent<Player>();
        p.collectible(typeUpdate, value);
        Destroy(gameObject);
    }

}
