﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour
{
    public Image panelWin;
    public Animator charAnimator;

    void Start()
    {
    }

    private void OnTriggerEnter()
    {
        Debug.Log("collision");
        charAnimator.SetBool("win", true);
        panelWin.gameObject.SetActive(true);
    }
}
