﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAI : MonoBehaviour
{
	    //Distance entre le joueur et l'ennemi
    private float Distance;
 
    // Cible de l'ennemi
    public Transform Target;
 
    //Distance de poursuite
    public float chaseRange = 10;
 
    // Portée des attaques
    public float attackRange = 2.2f;
 
    // Cooldown des attaques
    public float attackRepeatTime = 3;
    private float attackTime;
 
    // Montant des dégâts infligés
    public float TheDammage = 1;
 
    // Agent de navigation
    private UnityEngine.AI.NavMeshAgent agent;

    // Animations de l'ennemi
    //private Animation animations;

    public Animator golemAnimator;
    // Vie du golem
    public int health = 10;
    private bool isDead = false;



    void Start () {
        agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        golemAnimator = GetComponent<Animator>();
        //animations = gameObject.GetComponent<Animation>();
        attackTime = Time.time;
    }
 
 
 
    void Update () {
 
        if (!isDead)
        {
 
            // On cherche le joueur en permanence
            //Target = GameObject.Find("Player").transform;
 
            // On calcule la distance entre le joueur et l'ennemi, en fonction de cette distance on effectue diverses actions
            Distance = Vector3.Distance(Target.position, transform.position);
 
            // Quand l'ennemi est loin = idle
            if (Distance > chaseRange)
            {
                idle();
            }
 
            // Quand l'ennemi est proche mais pas assez pour attaquer
            if (Distance < chaseRange && Distance > attackRange)
            {
                chase();
            }
 
            // Quand l'ennemi est assez proche pour attaquer
            if (Distance < attackRange)
            {
                attack();
            }
 
        }
    }
 
    // poursuite
    void chase()
    {
        golemAnimator.SetBool("isWalking", true);
        agent.destination = Target.position;
    }
 
    // Combat
    void attack()
    {
        // empeche l'ennemi de traverser le joueur
        agent.destination = transform.position;
            Player Targ = Target.GetComponent<Player>();

        //Si pas de cooldown
        if (Time.time > attackTime)
        {
            golemAnimator.SetBool("attack", true);
            //animations.Play("hit");
            Debug.Log("L'ennemi a envoyé " + TheDammage + " points de dégâts");
            Targ.takeDammage(1);
            attackTime = Time.time + attackRepeatTime;
        }
        else
        {
            golemAnimator.SetBool("attack", false);
        }
    }
 
    // idle
    void idle()
    {
        golemAnimator.SetBool("isWalking", false);
    }
 
    public void takeDammage(int TheDammage)
    {
        if (!isDead)
        {
            health = health - TheDammage;
            print("Le golem a subit " + TheDammage + " points de dégâts.");
 
            if(health <= 0)
            {
                Dead();
            }
        }
    }
 
    public void Dead()
    {
        isDead = true;
        //animations.Play("die");
        Destroy(transform.gameObject, 5);
    }
}
