﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    [SerializeField]
    private float speed = 1f;
    private int health = 10;
    private int healthMax = 10;
    private int endurance = 1000;
    private int enduranceMax = 1000;
    private int faim = 10;
    private bool canRun = true;
    private float speedCam = 2f;
    private bool isLose = false;
    public Text healthText;
    public Text enduranceText;
    public Text faimText;
    public Image panelLose;
    public Animator charAnimator;


    public Transform Target;

    void Start()
    {
        charAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        bool move = false;

        if (!isLose)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Vector3 moveVector = Vector3.zero;
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Z))
            {
                move = true;
                moveVector.z = 1f;
                transform.position += transform.forward * Time.deltaTime * speed;
            }
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                move = true;
                moveVector.z = -1f;
                transform.position += -transform.forward * Time.deltaTime * speed;
            }

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                move = true;
                moveVector.x = 1f;
                transform.position += transform.right * Time.deltaTime * speed;
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Q))
            {
                move = true;
                moveVector.x = -1f;
                transform.position += -transform.right * Time.deltaTime * speed;
            }

            if (move)
                charAnimator.SetBool("isWalking", true);
            else
                charAnimator.SetBool("isWalking", false);

            updateEndurance();

            if (moveVector == Vector3.zero)
            {
                return;
            }

            transform.Rotate(0, Input.GetAxis("Mouse X") * speedCam, 0);
        }
    }

    private void updateEndurance()
    {
        if ((Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift)) && endurance > 0 && canRun)
        {
            charAnimator.SetBool("isRunning", true);
            speed = 3f;
            endurance -= 1;

            if (faim > 0 && endurance == 0)
            {
                faim -= 1;
                faimText.text = "Faim : " + faim + " / 10";
            }

            if (endurance == 0 || faim == 0)
                canRun = false;
        }
        else
        {
            charAnimator.SetBool("isRunning", false);
            speed = 1f;

            if (endurance < enduranceMax)
                endurance += 1;

            if (endurance > enduranceMax/10 && faim > 0)
                canRun = true;
        }

        enduranceText.text = "Endurance : " + endurance + " / " + enduranceMax;
    }

    public void takeDammage(int value)
    {
        if (health > 0)
        {
            health -= value;
            healthText.text = "Santé : " + health + " / " + healthMax;
            if (health == 0)
            {
                charAnimator.SetBool("isDead", true);
                panelLose.gameObject.SetActive(true);
                isLose = true;
            }
        }
    }

    public void collectible(string type, int value)
    {
        if (type == "faim")
        {
            endurance = enduranceMax;
            updateEndurance();
            if (faim < 10)
            {
                faim += value;
            } else
            {
                Debug.Log("Vous n'avez plus faim");
            }
            faimText.text = "Faim  : " + faim + " / 10";
        }
        if (type == "health")
        {
            if (health < 10)
            {
                health += value;
            }
            else
            {
                Debug.Log("Vous n'avez aucune blessure");
            }
            healthText.text = type + " : " + value + " / " + healthMax;
        }
    }

    private void takeHealth(int value)
    {
        health += value;
        healthText.text = "Santé : " + health + " / " + healthMax;
    }

    private void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision avec " + col.gameObject.name);
    }
}
