# survival-game-grapt

## Présentation du jeu

Notre jeu reprend le principe d'un jeu type labyrinthe tout en évitant les monstres qui s'y trouvent.

Le jeu se place dans une forêt où des golems rôdent afin de s'attaquer à vous !

Pour gagner il suffit de trouver la statue cachée au centre de la forêt.

## Contrôles du jeu

Les contrôles du jeu sont très simples :

Il suffit de se déplacer à l'aide des flèches directionnelles ou bien des touches ZQSD.

Il est également possible de courir à l'aide des touches Shift.

## Règles du jeu

Le joueur commence avec une jauge de faim à 10/10, une jauge de vie à 10/10 et une jauge d'endurance à 1000/1000.

Lorsque l'endurance tombe à 0 la faim décrémente de 1.

Lorsque la faim tombe à 0 il n'est plus possible de courir.

Lorsque l'endurance tombe à 0 il faut attendre que celle-ci remonte à 100 pour recommencer à courir.

Si la vie tombe à 0 la partie est perdue.

Des pommes sont présentes à plusieurs endroits sur la map, toucher ces pommes rendent 1 de faim.